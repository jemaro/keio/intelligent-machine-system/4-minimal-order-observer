Andreu Gimenez Bolinches ([andreu@keio.jp](mailto:andreu@keio.jp))

# Problem

Consider the system given by the following state space equation:

```math
\dot{x} = Ax + Bu
```

```math
x = 
\left[\begin{matrix}x_1\\x_2\\x_3\\x_4\end{matrix}\right] =
\left[\begin{matrix}x\\\theta\\\dot{x}\\\dot{\theta}\end{matrix}\right]

```

```math
\left[\begin{matrix}\dot{x_1}\\\dot{x_2}\\\dot{x_3}\\\dot{x_4}\end{matrix}\right] =
\left[\begin{matrix}0 & 0 & 1 & 0\\0 & 0 & 0 & 1\\0 & - \frac{3 g m}{4 M + m}
& 0 & 0\\0 & \frac{g \left(3 M + 3 m\right)}{l \left(4 M + m\right)} & 0 &
0\end{matrix}\right]
\left[\begin{matrix}x_1\\x_2\\x_3\\x_4\end{matrix}\right]
+ 
\left[\begin{matrix}0\\0\\\frac{4}{4 M + m}\\- \frac{3}{l \left(4 M +
m\right)}\end{matrix}\right] u

```

```math
y = Cx
```

```math
\left[\begin{matrix}y_1\\y_2\\\end{matrix}\right] =
\left[\begin{matrix}1 & 0 & 0 & 0\\0 & 1 & 0 & 0\\\end{matrix}\right]
\left[\begin{matrix}x\\\theta\\\dot{x}\\\dot{\theta}\end{matrix}\right]

```

Find a minimal order observer so that pole of observer is -10, 10 and $`g=9.8`$.
Please set the value of each parameter $`m`$, $`M`$ and $`l`$ appropriately.

## Assumptions

If we consider $`x`$ to be a distance unit $`[m]`$ and $`\theta`$ an angle $`[rad]`$
then an analysis of the state space equation reveals that $`m`$ and $`M`$ must be
masses $`[kg]`$ and $`l`$ must be a distance $`[m]`$ while $`u`$ must be a force
$`[kg*m/s^2]=[N]`$

```math
m > 0 
```
```math
M > 0 
```
```math
l > 0 
```

## Approach

The problem has been solved using an interactive Python notebook, which has
been then converted into `pdf` as a report. One can find the source notebook in
the [code
repository](https://gitlab.com/jemaro/keio/intelligent-machine-system/4-minimal-order-observer).

Initially we will import some general libraries and define the
[problem](#problem) system.


```python
import numpy as np
import control as ct
from sympy import *
from IPython.display import display, Math

init_printing()

# Printing function
def disp(*expresions: list):
    string = ''
    for expr in expresions:
        if isinstance(expr, str):
            string += expr
        elif isinstance(expr, np.ndarray):
            string += latex(Matrix(expr))
        else:
            raise NotImplementedError(type(expr))
    display(Math(string))

```


```python
def system(m: float = 3, M: float = 10, l: float = 3, g: float = 9.8):
    A = np.array([
        [0, 0, 1, 0],
        [0, 0, 0, 1],
        [0, (-3 * m * g) / (m + 4 * M), 0, 0],
        [0, 3 * (M + m) * g / ((m + 4 * M) * l), 0, 0],
        ])
    B = np.array([
        [0],
        [0],
        [4 / (m + 4 * M)],
        [-3 / ((m + 4 * M) * l)],
        ])
    C = np.array([[1, 0, 0, 0], [0, 1, 0, 0]])  # Output matrix
    D = np.zeros(shape=(C.shape[0], B.shape[1]))
    return ct.StateSpace(A, B, C, D)
sys = system()
```

# Minimal order observer

```math
\dot{z} = \hat{A}x + ky + MBu
```
```math
\hat{x} = Dz + Hy
```

In order to solve the [problem](#problem), we will follow the steps outlined in
lecture 8-6 and 8-7.

## Step 1
```math
S = \left[\begin{matrix}C\\W\end{matrix}\right]
```
```math
SAS^{-1} = \left[\begin{matrix}A_{11}&A_{12}\\A_{21}&A_{22}\end{matrix}\right]
```
```math
SB = \left[\begin{matrix}B_1\\B_2\end{matrix}\right]
```


```python
r, n = sys.C.shape

# Try a random W until we find an invertible S
for iter in range(10):
    W = np.random.randint(low=0, high=2, size=(n - r, n))
    S = np.vstack([sys.C, W])
    try:
        S_1 = np.linalg.inv(S)
    except np.linalg.LinAlgError as e:
        if 'Singular matrix' in str(e):
            continue
        else:
            raise
    break
else:
    raise RuntimeError(f'Maximum number of iterations reached: {iter}')
disp('S=', S)
SAS = S @ sys.A @ S_1
disp('SAS^{-1}=', SAS)
(A11, A12), (A21, A22) = (np.hsplit(a, [r]) for a in np.vsplit(SAS, [r]))
SB = S @ sys.B
disp('SB=', SB)
B1, B2 = np.vsplit(SB, [r])
```


$`\displaystyle S=\left[\begin{matrix}1.0 & 0 & 0 & 0\\0 & 1.0 & 0 & 0\\1.0 & 0 & 0 & 1.0\\1.0 & 0 & 1.0 & 1.0\end{matrix}\right]`$



$`\displaystyle SAS^{-1}=\left[\begin{matrix}0 & 0 & -1.0 & 1.0\\-1.0 & 0 & 1.0 & 0\\0 & 2.96279069767442 & -1.0 & 1.0\\0 & 0.911627906976745 & -1.0 & 1.0\end{matrix}\right]`$



$`\displaystyle SB=\left[\begin{matrix}0\\0\\-0.0232558139534884\\0.0697674418604651\end{matrix}\right]`$


## Step 2

We define $`\hat{A}`$ taking into account the desired poles of the observer.

```math
\hat{A} = A_{22} - LA_{12}
```


```python
Ahat = np.diag([-10, -10])
L = (A22 - Ahat) @ np.linalg.inv(A12)
disp('L=', L)
```


$`\displaystyle L=\left[\begin{matrix}1.0 & 10.0\\11.0 & 10.0\end{matrix}\right]`$


## Step 3

```math
k = \hat{A}L + A_{21} - LA_{11}
```
```math
D = S^{-1} \left[\begin{matrix}0\\I_{n-r}\end{matrix}\right]
```
```math
H = S^{-1} \left[\begin{matrix}I_r\\L\end{matrix}\right]
```
```math
MB = -LB_1 + B_2
```
```math
M = \left[\begin{matrix}-L&I_{n-r}\end{matrix}\right]S
```


```python
k = Ahat @ L + A21 - L @ A11
disp('k=', k)
D = S_1 @ np.vstack([np.zeros((r, r)), np.eye(n - r)])
disp('D=', D)
H = S_1 @ np.vstack([np.eye(r), L])
disp('H=', H)
MB = -L @ B1 + B2
disp('MB=', MB)
M = np.hstack([-L, np.eye(n-r)]) @ S
disp('M=', M)
```


$`\displaystyle k=\left[\begin{matrix}0 & -97.0372093023256\\-100.0 & -99.0883720930233\end{matrix}\right]`$



$`\displaystyle D=\left[\begin{matrix}0 & 0\\0 & 0\\-1.0 & 1.0\\1.0 & 0\end{matrix}\right]`$



$`\displaystyle H=\left[\begin{matrix}1.0 & 0\\0 & 1.0\\10.0 & 0\\0 & 10.0\end{matrix}\right]`$



$`\displaystyle MB=\left[\begin{matrix}-0.0232558139534884\\0.0697674418604651\end{matrix}\right]`$



$`\displaystyle M=\left[\begin{matrix}0 & -10.0 & 0 & 1.0\\-10.0 & -10.0 & 1.0 & 1.0\end{matrix}\right]`$


## Result

```math
\dot{z} = \hat{A}x + ky + MBu
```
```math
\hat{x} = Dz + Hy
```


```python
disp('\dot{z}=', Ahat, 'z + ', k, 'y + ', MB, 'u')
disp('\hat{x}=', D, 'z + ', H, 'y')
```


$`\displaystyle \dot{z}=\left[\begin{matrix}-10 & 0\\0 & -10\end{matrix}\right]z + \left[\begin{matrix}0 & -97.0372093023256\\-100.0 & -99.0883720930233\end{matrix}\right]y + \left[\begin{matrix}-0.0232558139534884\\0.0697674418604651\end{matrix}\right]u`$



$`\displaystyle \hat{x}=\left[\begin{matrix}0 & 0\\0 & 0\\-1.0 & 1.0\\1.0 & 0\end{matrix}\right]z + \left[\begin{matrix}1.0 & 0\\0 & 1.0\\10.0 & 0\\0 & 10.0\end{matrix}\right]y`$


Finally, we check that our solution fullfills the required conditions to guarantee $`(e \rightarrow 0)`$:


```python
disp('\hat{A}M=', Ahat @ M, '= MA - kC =', M @ sys.A - k @ sys.C)
disp('I_n=', np.eye(n), '= DM + HC =', D @ M + H @ sys.C)
disp('\hat{A}: stable,\quad eigenvalues(\hat{A}) =', np.linalg.eigvals(Ahat))
```


$`\displaystyle \hat{A}M=\left[\begin{matrix}0 & 100.0 & 0 & -10.0\\100.0 & 100.0 & -10.0 & -10.0\end{matrix}\right]= MA - kC =\left[\begin{matrix}0 & 100.0 & 0 & -10.0\\100.0 & 100.0 & -10.0 & -10.0\end{matrix}\right]`$



$`\displaystyle I_n=\left[\begin{matrix}1.0 & 0 & 0 & 0\\0 & 1.0 & 0 & 0\\0 & 0 & 1.0 & 0\\0 & 0 & 0 & 1.0\end{matrix}\right]= DM + HC =\left[\begin{matrix}1.0 & 0 & 0 & 0\\0 & 1.0 & 0 & 0\\0 & 0 & 1.0 & 0\\0 & 0 & 0 & 1.0\end{matrix}\right]`$



$`\displaystyle \hat{A}: stable,\quad eigenvalues(\hat{A}) =\left[\begin{matrix}-10.0\\-10.0\end{matrix}\right]`$

